import React from "react";

export const Navigation = props => {
  const languages = ["all", "javascript", "java", "ruby"];
  
  return (
    <nav>
      <ul>
        {languages.map(lang => (
          <li key={lang} onClick={() => props.onSelectLanguage(lang)}>
            {lang}
          </li>
        ))}
      </ul>
    </nav>
  );
};
