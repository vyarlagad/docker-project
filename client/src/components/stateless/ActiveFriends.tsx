export const ActiveFriends = props => {
  return (
    <div>
      <h2>Active Friends</h2>
      <ul>
        {props.list.map(friend => (
          /* Added Perentheses because Multi Line Return statement
          on map (Returns Individual Element to the array)
          */
          <li key={friend.name}>
            <span> {friend.name} </span>
            <button onClick={() => props.onRemoveFriend(friend.name)}>
              Remove Friend
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};
