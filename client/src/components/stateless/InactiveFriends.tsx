import React from "react";

export const InactiveFriends = props => {
  return (
    <div>
      <h2>Inactive Friends</h2>
      <ul>
        {props.list.map(friend => (
          /* Added Perentheses because Multi Line Return statement
            on map (Returns Individual Element to the array)
            */
          <li key={friend.name}>
            <span> {friend.name} </span>
            <button onClick={() => props.onToggleFriend(friend.name)}>
              Activate Friend
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};
