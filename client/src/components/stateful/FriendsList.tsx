import { ActiveFriends } from "./components/stateless/ActiveFriends";
import { InactiveFriends} from "./components/stateless/InactiveFriends"

export default class FriendsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      friends: [
        {
          name: "Jordyn",
          active: true
        },
        {
          name: "Tobby",
          active: true
        },
        {
          name: "Michael",
          active: false
        }
      ],
      input: ""
    };
  }
  // Life Cycle Methods
  componentDidMount() {
    console.log("--componentDidMount--");
  }
  componentDidUpdate() {
    console.log("--componentDidUpdate--");
  }
  componentWillUnmount() {
    console.log("--componentWillUnmount--");
  }

  handleAddFriend = e => {
    this.setState(prevState => {
      return {
        friends: prevState.friends.concat([
          {
            name: prevState.input,
            active: true
          }
        ]),
        input: ""
      };
    });
  };

  handleRemoveFriend = name => {
    this.setState(prevState => {
      return {
        // this is brackets because we are returning an object
        friends: prevState.friends.filter(friend => friend.name !== name)
      };
    });
  };

  handleToggleFriend = name => {
    console.log("Toggle Friend Activated");
    this.setState(prevState => {
      // get the friend which we want to activate
      const friend = prevState.friends.find(friend => friend.name === name);
      return {
        friends: prevState.friends
          .filter(friend => friend.name !== name)
          .concat([
            {
              name,
              active: !friend.active
            }
          ])
      };
    });
  };

  updateInput = e => {
    const value = e.target.value;
    // We don't care about the previous state of input so we just pass object
    this.setState({
      input: value
    });
  };

  handleClearAll = () => {
    this.setState({
      friends: []
    });
  };

  render() {
    return (
      <ReactFragment>
        <input
          type="text"
          placeholder="New friend"
          value={this.state.input}
          onChange={this.updateInput}
        />
        <button onClick={this.handleAddFriend}> Submit </button>
        <button onClick={this.handleClearAll}>Clear All</button>
        <ActiveFriends
          list={this.state.friends.filter(friend => friend.active === true)}
          onRemoveFriend={this.handleRemoveFriend}
          onToggleFriend={this.handleToggleFriend}
        />
        <InactiveFriends
          list={this.state.friends.filter(friend => friend.active !== true)}
          onToggleFriend={this.handleToggleFriend}
        />
      </ReactFragment>
    );
  }
}
