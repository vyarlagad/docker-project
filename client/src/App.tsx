import React, { Component } from "react";
import { Navigation } from "./components/stateless/Navigation";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      repos: [],
      activeLanguage: "all",
      loading: true
    };
  }

  handleActiveLanguage = lang => {
    // don't care about previous state
    this.setState({
      activeLanguage: lang
    });
  };

  fetchRepos = (lang) =>{
    this.setState({
      loading: true
    })
  }

  render() {
    return (
      <div className="App">
        <Navigation onSelectLanguage = {this.handleActiveLanguage}/>
        <h1>Active Language: {this.state.activeLanguage}</h1>
      </div>
    );
  }
}

export default App;
